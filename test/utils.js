'use strict'

const Validator  = require('jsonschema').Validator;
var v = new Validator();

exports.validate = function(json, schema){
    let validateSchema = v.validate(json, schema)
    if (validateSchema.errors.length > 0)
        throw new Error(validateSchema.errors)
}