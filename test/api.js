'use strict'

const app                = require("./../server");
const request            = require("supertest")(app);
const utils              = require('./utils')
const listSchema         = require('./../server/src/schemas/list')
const checkPackageSchema = require('./../server/src/schemas/checkPackage')
const infoPackageSchema = require('./../server/src/schemas/infoPackage')

// describe('GET /packages/list.json', function () {
//     it('respond with ok json', function (done) {
//         request
//             .get('/packages/list.json')
//             .set('Accept', 'application/json')
//             .expect('Content-Type', /json/)
//             .expect(200)
//             .end(function (err, res) {
//                 if (err) return done(err);
//                 utils.validate(res.body, listSchema)
//                 done();
//             });
//     });
// });

// describe('GET /packages/download/https%3A%2F%2Fgithub.com%2Fsymfony%2Fsymfony.git', function () {
//     it('respond with ok json', function (done) {
//         this.timeout(20000);
//         request
//             .get('/packages/check/https%3A%2F%2Fgithub.com%2Fsymfony%2Fsymfony.git')
//             .set('Accept', 'application/json')
//             .expect('Content-Type', /json/)
//             .expect(200)
//             .end(function (err, res) {
//                 if (err) return done(err);
//                 if (res.body.data)
//                     utils.validate(res.body, checkPackageSchema)
//                 done();
//             });
//     });
// });

// describe('GET /packages/create', function () {
//     it('respond with ok json', function (done) {
//         let data = {
//             repository: "git@bitbucket.org:gigigo/gigigo-internal-infomix-bundle-symfony.git",
//             type: "vcs"
//         }
//         this.timeout(20000);
//         request
//             .post('/packages/create')
//             .set('Accept', 'application/json')
//             .send(data)
//             .expect('Content-Type', /json/)
//             .expect(200)
//             .end(function (err, res) {
//                 if (err) return done(err);
//                 utils.validate(res.body, checkPackageSchema)
//                 done();
//             });
//     });
// });


describe('GET /packages/fourcoders/generatecrud-bundle.json', function () {
    it('respond with ok json', function (done) {
        this.timeout(20000);
        request
            .get('/packages/fourcoders/generatecrud-bundle.json')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                if (err) return done(err);
                console.log(res.body)
                utils.validate(res.body.data, infoPackageSchema)
                done();
            });
    });

});

