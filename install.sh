#!/bin/bash

echo "- Move to satis directory..."
cd ./node_modules/satis/
echo "- Installing dependences..."
./../getcomposer/composer.phar install
echo "- Move to root directory project..." 
cd ./../../
echo "- Generate index..." 
./node_modules/satis/bin/satis build ./data/satis.json ./build