'use strict'

const Vue        = require('vue')
const Vuex       = require('vuex')
const VueRouter  = require('vue-router')
const App        = require('./App.vue')

const ListPackages  = require('./components/Packages/List.vue')
const CreatePackage = require('./components/Packages/Create.vue')
const EditPackage   = require('./components/Packages/Edit.vue')
const ShowPackage   = require('./components/Packages/Show.vue')
const About         = require('./components/About.vue')
const Vendor        = require('./components/Vendor.vue')
const Footer        = require('./components/Footer.vue')
const Api           = require('./components/Api.vue')
const Statistics    = require('./components/Statistics.vue')
const Runner        = require('./components/Runner.vue')
const Settings      = require('./components/Settings.vue')

Vue.use(Vuex)
Vue.use(VueRouter)

const routes = [
    { path: '/', component: ListPackages, name: 'list' },
    { path: '/search', component: ListPackages, name: 'search' },
    { path: '/submit', component: CreatePackage },
    { path: '/about', component: About },
    { path: '/rest', component: Api },
    { path: '/statistics', component: Statistics },
    { path: '/settings', component: Settings },
    { path: '/runner', component: Runner },
    { path: '/packages/:vendor', name: 'vendor', component: Vendor },
    { path: '/packages/:vendor/:project', name: 'package', component: ShowPackage },
    { path: '/packages/:vendor/:project/edit', name: 'edit', component: EditPackage }    
]

const router = new VueRouter({
    mode: 'history',
    scrollBehavior: function(to, from, savedPosition) {
        if (to.hash) {
            return {selector: to.hash}
        } else {
            return { x: 0, y: 0 }
        }
    },
    routes
})

router.beforeEach((to, from, next) => {
    next()
})

const store = new Vuex.Store({
    state: {
        buildsRunner: [],
        textSearch: ''
    },
    mutations: {
        addElement (state, element) {
            state.buildsRunner.push(element)
        },
        newSearch(state, string){
            state.textSearch = string
        },
        clearSearch(state){
            state.textSearch = ''
        }
    }
})

const key = "vuex.state"

try{
    let lastState = JSON.parse(window.localStorage.getItem(key));
    store.replaceState( Object.assign({},store.state,lastState) )
}catch(err){
    console.error('Error restoring vuex state',err)
}
store.subscribe((mutation,state)=>{
    window.localStorage.setItem(key, JSON.stringify(state))
})

// Main app
new Vue({
    router,
    store,
    el: '#app',
    render: h => h(App)
})
