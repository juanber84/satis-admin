'use strict'

module.exports = function(){
    if (!module.exports.io) {

        let opts
        if (process.env && !process.env.NODE_ENV) {
            var parameters = require('./../../../parameters')
            opts = `http://${parameters.apps.api.host}:${parameters.apps.api.listenPort}`
        }

        let io = module.exports.io  = require('socket.io-client')(opts); 
    }
}