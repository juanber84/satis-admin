'use strict'

var axios = require('axios')
var co = require('co')

let opts = {
    timeout: 20000
}

if (process.env && !process.env.NODE_ENV) {
    var parameters = require('./../../../parameters')
    opts.baseURL = `http://${parameters.apps.api.host}:${parameters.apps.api.listenPort}`
}

var request = axios.create(opts)

module.exports = {

    getPackages() {
        return co(function* () {
            let responsePackages = yield request.get('/packages.json')
            if (responsePackages.status == 200 && responsePackages.data) {
                let tokenPackagesInfo = Object.keys(responsePackages.data.includes)[0]
                let infoPackages = yield request.get('/' + tokenPackagesInfo)
                return infoPackages.data
            } else {
                throw Error('404')
            }
        })
    },

    list() {
        return request.get('/api/list')
            .then((response) => {
                if (response.status == 200 && response.data && response.data.status == true) {
                    return response.data.data
                } else {
                    throw Error('404')
                }
            })
    },










    listAllPackages() {
        return request.get('/api/packages/list.json')
            .then((response) => {
                if (response.status == 200 && response.data && response.data.status == true) {
                    return response.data.data
                } else {
                    throw Error('404')
                }
            })
    },

    listByOrganization() {
        return request.get('/api/packages/list.json?vendor=composer')
            .then((response) => {
                if (response.status == 200 && response.data && response.data.status == true) {
                    return response.data.data
                } else {
                    throw Error('404')
                }
            })
    },

    listByType() {
        return request.get('/api/packages/list.json?type=composer-plugin')
            .then((response) => {
                if (response.status == 200 && response.data && response.data.status == true) {
                    return response.data.data
                } else {
                    throw Error('404')
                }
            })
    },


    GettingPackageData(vendor, project, version) {
        let url = `/api/packages/${vendor}/${project}.json`
        if (version)
            url = url + `?version=${version}`
        return request.get(url)
            .then((response) => {
                if (response.status == 200 && response.data && response.data.status == true) {
                    return response.data.data.package
                } else {
                    throw Error('404')
                }
            })
    },

    checkRepository(repository) {
        return request.get(`/api/packages/check/${repository}`)
            .then((response) => {
                if (response.status == 200 && response.data && response.data.status == true) {
                    return response.data.data
                } else {
                    throw Error('404')
                }
            })
    },

    saveRepository(data) {
        return request.post(`/api/packages/create`, data)
            .then((response) => {
                if (response.status == 200 && response.data && response.data.status == true) {
                    return response.data.data
                } else {
                    throw Error('404')
                }
            })
    },

    DeletePackage(vendor, project) {
        let data = {
            vendor: vendor, 
            project: project
        }
        return request.post(`/api/packages/delete`, data)
            .then((response) => {
                if (response.status == 200 && response.data && response.data.status == true) {
                    return true
                } else {
                    throw Error('404')
                }
            })
    }

}