'use strict'

const debug   = require('debug')('app:src:lib:builder')
const Promise = require('bluebird')
const spawn   = require('child_process').spawn;
const io      = require('./../services/ws').io;

// TODO pass repositoryName
exports.build = function(repositoryName){

    return new Promise(function(resolve, reject){

        let opts = [
            'build', 
            'data/satis.json', 
            './build',
            '-vv'
        ] 
        if (repositoryName) {
            opts.push('--repository-url')
            opts.push(repositoryName)
        }

        const build = spawn('./node_modules/satis/bin/satis', opts);

        build.stdout.on('data', (data) => {
            data = data.toString().replace(/(\r\n|\n|\r)/gm,"").trim().replace(" ","-")
            debug('info', data, repositoryName ? repositoryName : 'all')
            io.sockets.emit('task', { message: data, repositoryName: repositoryName })
        });

        build.stderr.on('data', (data) => {
            data = data.toString().replace(/(\r\n|\n|\r)/gm,"").trim().replace(" ","-")
            debug('info', data, repositoryName ? repositoryName : 'all')
            io.sockets.emit('task', { message: data, repositoryName: repositoryName })
        });

        build.on('close', (code) => {
            io.sockets.emit('task', { end: true, repositoryName: repositoryName })
            resolve()
        });

        build.on('error', (err) => {
            reject(err)
        });

    })
}

