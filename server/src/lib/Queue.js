'use strict'

const debug   = require('debug')('app:services:queue')
const Promise = require('bluebird')
const EventEmitter = require('events').EventEmitter

class Queue extends EventEmitter {

    constructor(opts) {
        super()
        this.stack = []
        this.processing = []
        this.auto = (opts && opts.auto) ? !!opts.auto : false
        this.setConcurrence(opts && opts.concurrence)
    }

    setConcurrence(value){
        this.concurrence = Math.max(1, value | 0)
        this.start()
    }

    push(item) {
        this.stack.push(item)
        if (this.auto)
            this.next()
    }

    pop() {
        return this.stack.pop()
    }

    shift() {
        return this.stack.shift()
    }

    extract(){
        // TODO
    }

    count(){
        return this.stack.length
    }

    next(all){
        if (this.stack.length > 0 && !all && this.processing.length < this.concurrence) {
            let task = this.stack.shift()
            this.processing.push(task)
            Promise.resolve(task())
                .finally(() => {
                    this.processing.splice(this.processing.indexOf(task),1)
                    this.start()
                })
        } else if (this.stack.length == 0) {
            this.emit('queue-empty')
        }
    }

    start(){
        for (var i = 0; i < this.concurrence; i++) {
            this.next()
        }
    }

}

module.exports = Queue