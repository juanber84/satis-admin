'use strict'

const debug      = require('debug')('app:src:services:data')
const fs         = require('co-fs')
const _          = require('lodash')
const parameters = require('./../../../parameters')
const task       = require('./task')
const git        = require('./git')

module.exports = {

    *list(){
        // get package.json info
        let packageJsonData = yield _getPackageFileContent()
        // get dinamic key include
        let keyIncludeJson = Object.keys(packageJsonData.includes)[0]
        // get include.json info
        let includeJsonData = yield _getIncludeFileContent(keyIncludeJson)
        // Generate array results
        let results = _normalizeListResults(includeJsonData)
        // Return results
        return {
            results: results,
            total: results.length,
        } 
    },

    *show(parameters, version){
        // get package.json info
        let packageJsonData = yield _getPackageFileContent()
        // get dinamic key include
        let keyIncludeJson = Object.keys(packageJsonData.includes)[0]
        // get include.json info
        let includeJsonData = yield _getIncludeFileContent(keyIncludeJson)
        // Get an array of my packages names
        let myPackages = Object.keys(includeJsonData.packages)
        // get package info
        let packageInfo = includeJsonData.packages[`${parameters.vendor}/${parameters.project}`]
        // get version of results
        if (!version) 
            version = Object.keys(packageInfo)[0]
        let versionInfo = Object.assign({},packageInfo[version])
        // Set version to show
        versionInfo.infoVersion = version
        // Generate results
        let results = _normalizeShowResults(versionInfo, packageInfo)
        
        // Get readem and composer
        let reference = versionInfo.versions[version].source.reference // TODO validate
        let readme = yield git.show(versionInfo.source.url, reference, 'README.md')
        let composerJson = yield git.show(versionInfo.source.url, reference, 'composer.json')
        // Normalize composer json
        if (composerJson)
            composerJson = JSON.parse(composerJson)

        // Merge information
        versionInfo.readme = readme || ''
        versionInfo.require = composerJson.require || {}
        versionInfo.requireDev = composerJson['require-dev'] || {}
        versionInfo.suggest = composerJson.suggest || {}
        versionInfo.provide = composerJson.provide || {}
        versionInfo.replace = composerJson.replace || {}
        versionInfo.conflict = composerJson.conflict || {}
        versionInfo.mantainer = {
            img: ""
        }
        versionInfo.myPackages = myPackages

        return {
            package: versionInfo
        } 
    },

    *getInfoAll(){
        let fileData = yield fs.readFile(file, 'utf8')
        let jsonData = JSON.parse(fileData)
        return jsonData
    },

    *getInfoRepositories(){
        let jsonData = yield module.exports.getInfoAll()
        return jsonData.repositories
    },

    *getInfoRepository(repositoryName){
        let repositories = yield module.exports.getInfoRepositories()
        return _.find(repositories, { 'url':  repositoryName })
    },

    *update(data){
        try {
            yield fs.writeFile(file, JSON.stringify(data, null, 4),  'utf8')
        } catch (err) {
            throw err
        }
        return data
    },

    *create(data){
        try {
            // Get satis.json file data
            let jsonData = yield _getSatisFileContent()
            debug('get-repositories')
            // get old repositories
            let repositories = jsonData.repositories
            debug('add-repository')
            // Add new repositories
            repositories.push(data)
            debug('clean-repositories')
            // Clean duplicates
            jsonData.repositories = _.uniqBy(repositories, 'url')
            // Insert and save
            yield _setSatisFileContent(jsonData)
            debug('fetch-repository')
            // Update repository
            yield git.fetch(data.url)
            // Build package
            task.buildOne(data.url)
        } catch (err) {
            throw err
        }
        
        return true
    },

    *delete(data){
        // get package.json info
        let packageJsonData = yield _getPackageFileContent()
        // get dinamic key include
        let keyIncludeJson = Object.keys(packageJsonData.includes)[0]     
        // get include.json info
        let includeJsonData = yield _getIncludeFileContent(keyIncludeJson)
        // Delete key
        delete includeJsonData.packages[`${data.vendor}/${data.project}`]
        // Try write file of packages
        let includePackagesFile = __dirname + `/../../../build/${keyIncludeJson}`
        try {
            yield fs.writeFile(includePackagesFile, JSON.stringify(includeJsonData, null, 4),  'utf8')
        } catch (err) {
            throw err
        }
        return true
    },    

}

// Static functions
const file = __dirname + '/../../../data/satis.json'

function *_getSatisFileContent(){

    debug('get-satis.json-info')
    // Get file data
    var fileData = yield fs.readFile(file,  'utf8')
    // Tansform
    let jsonData = JSON.parse(fileData)

    return jsonData
}

function *_setSatisFileContent(data){

    debug('set-satis.json-info')
    // Set satis.json info
    try {
        yield fs.writeFile(file, JSON.stringify(data, null, 4),  'utf8')
    } catch (err) {
        throw err
    }
}

function *_getPackageFileContent(){

    debug('get-package.json-info')
    // Route of package json
    let packagesFile = __dirname + '/../../../build/packages.json'
    // get package.json info
    let packagesFileContent = yield fs.readFile(packagesFile, 'utf8')
    // Tansform
    let jsonData = JSON.parse(packagesFileContent)

    return jsonData
}

function *_getIncludeFileContent(key){

    debug('get-include.json-info')
    // get include.json info
    let includesFileContent = yield fs.readFile(__dirname + '/../../../build/' + key, 'utf8')
    // Tansform
    let jsonData = JSON.parse(includesFileContent)

    return jsonData
}

function _normalizeListResults(includeJsonData){
    debug('normalize-list-results')
    return _.values(_.mapValues(includeJsonData.packages, function(value, key){
        let dataLibrary = value[Object.keys(value)[0]]
        return {
            name: key,
            type: dataLibrary.type || "",
            description: dataLibrary.description,
            url: 'http://' + parameters.apps.api.host + ':' + parameters.apps.api.listenPort + '/packages/' + key,
            repository: _.get(dataLibrary, 'source.url'),
            downloads: 0,
            favers: 0,
        }
    }))
}

function _normalizeShowResults(versionInfo, packageInfo){
    debug('normalize-show-results')

    // Include all results
    versionInfo.versions = packageInfo
    // Custom results calculated
    versionInfo.repository = ''
    versionInfo.github_stars = 0
    versionInfo.github_watchers = 0
    versionInfo.github_forks = 0
    versionInfo.github_open_issues  = 0
    versionInfo.language = ''
    versionInfo.dependents = 0
    versionInfo.suggesters = 0
    versionInfo.downloads = []
    versionInfo.favers = 0
    versionInfo.installs = 0
    versionInfo.forks = 0

    return versionInfo
}