'use strict'

const debug     = require('debug')('app:src:services:git')
const _         = require('lodash')
const Promise   = require('bluebird')
const simpleGit = Promise.promisifyAll(require('simple-git')( __dirname + '/../../../packages' ));
const fs        = require('co-fs')

// Disable fatal error logs 
simpleGit.silent(true)

module.exports = {

    *fetch(repositoryName){
        // Path of README.md
        let path = __dirname + '/../../../packages/' + encodeURIComponent(repositoryName)        
        // Return content or false
        try {
            let gitInPath = Promise.promisifyAll(require('simple-git')(path));
            // Fetch all
            return yield gitInPath.fetchAsync(['--all'])
        } catch(err) {
            return false
        }        
    },

    *listRemote(repositoryName){
        try {
            yield simpleGit.listRemoteAsync([repositoryName])
            return true
        } catch(err) {
            return false
        }
    },

    *clone(repositoryName){

        try {
            debug('check', repositoryName)
            // Check if 
            let existRepository = yield fs.exists(__dirname + '/../../../packages/' + encodeURIComponent(repositoryName))

            // If repository not exist clone the repository
            if (!existRepository) {
                debug('clone')
                yield simpleGit.clone(repositoryName, encodeURIComponent(repositoryName))
                //debug('mirror')
                //yield simpleGit.mirror(repositoryName, encodeURIComponent(repositoryName))
            } else {
                debug('pull')
                let gitInPath = Promise.promisifyAll(require('simple-git')(__dirname + '/../../../packages/' + encodeURIComponent(repositoryName)));
                yield gitInPath.pull()
            }

            debug('get-info')
            // get package.json info
            let composerFileContent = yield fs.readFile(__dirname + '/../../../packages/' + encodeURIComponent(repositoryName) + '/composer.json', 'utf8')
            // Tansform
            let jsonData = JSON.parse(composerFileContent)

            // Return namespace of repository
            return {
                namespace: jsonData.name
            }

        } catch(err) {
            return false
        }
    },

    *show(repositoryName, version, file){

        debug('show', repositoryName)
        // Pre normalize master version        
        if (version == 'dev-master')
            version = 'master'
        if (version == 'dev-develop')
            version = 'develop'
        // Path of README.md
        let path = __dirname + '/../../../packages/' + encodeURIComponent(repositoryName)        
        // Return content or false
        try {
            let gitInPath = Promise.promisifyAll(require('simple-git')(path));
            // Fetch all
            // yield gitInPath.fetchAsync(['--all'])
            // Show file
            return yield gitInPath.showAsync([`${version}:${file}`])
        } catch(err) {
            return false
        }
    }



}
