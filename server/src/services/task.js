'use strict'

const debug         = require('debug')('app:services:task')
const builder       = require('./../lib/builder')
const Queue         = require('./../lib/Queue')
const QueueBuilders = new Queue({auto: true})

module.exports = {
    buildAll(){
        QueueBuilders.push(function(){
            return builder.build(null)
        })
    },
    buildOne(repositoryName){
        QueueBuilders.push(function(){
            return builder.build(repositoryName)
        })
    },
    total(){
        return QueueBuilders.count()
    }
}