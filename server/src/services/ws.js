'use strict'

const debug = require('debug')('app:src:services:ws')

module.exports = function(server){
    debug('init')
    if (!module.exports.io) {
        let io = module.exports.io  = require('socket.io')(server);
        io.on('connection', function (socket) {
            debug('client-connected', socket.id)
        });        
    }

}
