'use strict'

exports.apiInfo = [
    {
        method: 'GET',
        url: '/api/list',
        description: 'Return all projects configurated'
    },
    {
        method: 'POST',
        url: '/api/build/:repository',
        parameters: [
            {
                'name': ':repository',
                'mandatory': false
            }
        ],
        description: 'Build (all or only one) the projects configurated'
    },
]