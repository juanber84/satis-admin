'use strict';

const expressDeliver = require('express-deliver')
const _              = require('lodash')
const fs             = require('co-fs')
const debug          = require('debug')('app:controllers')

const exception = expressDeliver.exception

const axios = require('axios')

const request = axios.create({
    baseURL: 'https://packagist.org',
    timeout: 1000
});

module.exports = expressDeliver.wrapper({

    *list(req,res){
        return require('./../../mocks/list.json')
    },

    show(){
        return require('./../../mocks/show.json')
    }

})