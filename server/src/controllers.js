'use strict';

const debug          = require('debug')('app:src:controllers')
const expressDeliver = require('express-deliver')
const _              = require('lodash')
const doc            = require('./services/doc')
const git            = require('./services/git')
const data           = require('./services/data')
const task           = require('./services/task')

const exception = expressDeliver.exception

// QueueBuilders.on('queue-empty', () => {
//     console.log('queue-emptyx!');
// });

module.exports = expressDeliver.wrapper({

    index(req,res){
        // Return default api responto to live ping
        return 'hi'
    },

    info(req,res){
        // Return doc api
        return doc.apiInfo
    },

    *list(req,res){
        debug('list')
        // Return list packages
        return yield data.list()
    },

    *create(req,res){
        debug('create')
        let bodyData = {
            type: req.body.type || 'vcs',
            url: req.body.repository || ''           
        }
        // Return data info
        return yield data.create(bodyData)
    },

    *update(req,res){
        // Get data
        let bodyRequest = req.body
        // TODO validate data schema
        // Return data info
        return yield data.update(bodyRequest)
    },

    *delete(req,res){
        debug('delete')
        let parameters = { 
            vendor: req.body.vendor, 
            project: req.body.project
        }       
        // Return data info
        return yield data.delete(parameters)
    },

    *task(req,res){
        // Return data info
        return {
            task: task.count()
        }
    },

    *build(req,res){
        // Get repository name
        let repositoryName = (req.params.repository) ? decodeURIComponent(req.params.repository) : null
        // Control builder repositoryName
        if (repositoryName) {
            if (yield data.getInfoRepository(repositoryName))
                task.buildOne(repositoryName)
            else 
                return 'ko' 
        } else
            task.buildAll()
        // Return provisional response, the extra info will be process in async task
        return {
            task: task.count()
        }
    },

    *check(req,res){
        debug('check')
        // Get repository name
        let repositoryName = (req.params.repository) ? decodeURIComponent(req.params.repository) : null
        // Download repository
        if (repositoryName) {
            //return yield git.listRemote(repositoryName)
            return yield git.clone(repositoryName)            
        } else 
            return false 
    },

    *show(req,res){
        debug('show')
        // Get repository name
        let parameters = { 
            vendor: req.params.vendor, 
            project: req.params.project.slice(0, -5)
        }
        let version = req.query.version || null
        
        return yield data.show(parameters, version)
    }

})