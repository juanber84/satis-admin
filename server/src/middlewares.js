'use strict'

const debug          = require('debug')('app:src:middlewares')
const morgan         = require('morgan')
const cors           = require('cors')
const bodyParser     = require('body-parser')
const expressDeliver = require('express-deliver')
//const exception = require('express-deliver').exception

//Load custom exception
require('./customExceptions');

module.exports = function(app){
    
    //Disable express header
    app.set('x-powered-by',false)
    app.set('etag', false);

    //Simple request logger
    if (debug.enabled)
        app.use(morgan('dev'))

    //CORS options
    app.use(cors({
        origin : true, //Use the origin header in request
        maxAge : 600, 
        //exposedHeaders : ['Date','x-access-token'],
        //credentials:true
    }))

    //Responses based on promises
    app.use(expressDeliver)

    //Parses http body
    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(bodyParser.json())

    debug('init')
}