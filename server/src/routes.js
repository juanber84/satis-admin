'use strict';

const debug      = require('debug')('app:src:routes');
const express    = require('express');
const controller = require('./controllers');
const packagist  = require('./packagist');

module.exports = function(app){

    // Satis routing
    const front = express.static(__dirname + '/../../front')
    app.use("/", front)
    app.use("/search", front)
    app.use("/runner", front)
    app.use("/settings", front)
    app.use("/about", front)
    app.use("/submit", front)
    app.use("/rest", front)
    app.use("/statistics", front)
    app.use("/packages/:vendor/:project", front)
    app.use("/packages/:vendor/:project/edit", front)
    
    // Packagist routing
    let packagistRouter = express.Router()
    app.use('/api/packages', packagistRouter)

    packagistRouter.get('/list.json', controller.list);
    packagistRouter.post('/create', controller.create);
    packagistRouter.get('/check/:repository', controller.check);
    packagistRouter.get('/:vendor/:project', controller.show);
    packagistRouter.post('/delete', controller.delete);

    // Git routing
    app.use('/git/:repository/info/refs', function(req, res){
        let service = req.query.service
        res.send(service)
    })

    // Statics
    app.use(express.static(__dirname + '/../../build'))    

    debug('init')
};