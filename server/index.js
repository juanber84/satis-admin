'use strict'

const debug      = require('debug')('app:index');
const app        = require('express')();
const server     = require('http').Server(app);
const parameters = require('./../parameters');

require('./src/services/ws')(server)

require('./src/middlewares')(app);
require('./src/routes')(app);
require('./src/handlers')(app);

var port = parameters.apps.api.listenPort;

// If run in mocha disabled the listen
if (typeof describe == 'undefined') {
    server.listen(port,function(){
        debug('http-listening', port);
    });
}

module.exports = server;